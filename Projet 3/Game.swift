import Foundation
class Game {
//   Player team table.
    var teams: [Team] = []
    
//    function allowing 2 players to create each their team of characters.
    func createTeam()  {
        while teams.count < Config.teamsLimit {
            let team = Team()
            let teamNumber = teams.count + 1
            print("you are the team: \(teamNumber) ")
            team.createCharacter(players: teams)
            self.teams.append(team)
        }
    }
//    function initiating the battle phase between the 2 teams of players.
    func fighting()  {
        var loopTower = 0
        var fightingPlayer = teams[1]
        var targetPlayer = teams[0]
        var randomChest = Int(arc4random_uniform(5))
        repeat {
       
            
//        selection of the fighter.
            print("If you choose the magus you can heal one of your characters.")
            let fighter = fightingPlayer.selectCharacter()

//          The chest appears randomly.
            if randomChest == loopTower {
//           Weapon random choice.
                print("A chest appears in front of the character, he opens it and ... he equips himself with a new weapon!")
                fighter.newWeapon(character: fighter)
                print("You are equipped with a weapon of type: \(fighter.weapon.name) the weapon give you \(Int(fighter.weapon.damages)) more damages!")
                randomChest = Int(arc4random_uniform(6)) + 1
                loopTower = 0
            }
//          if the fighter is a magus heals a character from his team.
            if let magus = fighter as? Magus  {
                let targetHeal = fightingPlayer.selectCharacter()
                magus.healing(character: targetHeal)
                targetHeal.description()
            }else{      // selection of the target of the fighter in the advairse team.
                let target = targetPlayer.selectCharacter()
                fighter.fight(against: target)
                target.description()
            
                if target.life <= 0 {
                // Supression of the dead character in the teamMember table.
                    print("\(target.name) is dead.")
                    let index = targetPlayer.teamMember.index(where:{$0 === target})
                    targetPlayer.teamMember.remove(at: index!)
                }
                
                    loopTower += 1
                }
            swap(&fightingPlayer, &targetPlayer)
    //            Repeat until all the characters on a team are dead.
        }while teams[0].teamMember.count > 0 && teams[1].teamMember.count > 0

        //       Display of the winner.
        if teams[0].teamMember.isEmpty {
            print("The winner is team 2!")
        }
        else if teams[1].teamMember.isEmpty {
            print("The winner is team 1 !")
        }
    }
}


    







