class Fire: Types {

    override init() {
        super .init()
        name = "fire"
        weaknessesName = "water"
        strengthName = "wind"
    }
}
