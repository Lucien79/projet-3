class Earth: Types {
    
    override init() {
        super .init()
        name = "earth"
        weaknessesName = "wind"
        strengthName = "electric"
    }
}
