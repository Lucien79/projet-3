class Types {
    
    var name : String
    var weaknessesDamages = 0.5
    var strengthDamages = 1.5
    var weaknessesName: String
    var strengthName: String
    
    init() {
        name = ""
        weaknessesName = ""
        strengthName = ""
    }
}
