class Weapon {
    var name : String
    var damages : Double
    
    init(name: String, damages: Double) {
        self.name = name
        self.damages = damages
    }
}
