class Team {

//  Characters table in a team.
    var teamMember: [Character] = []

    //    function that allows players to create characters on their team.
    func createCharacter(players: [Team] )  {
        
        repeat {
            print("Choose a character from the character types: 1.Combatant, 2.Colossus, 3.Dwarf, 4.Magus")
        
        let answer = Int(readLine()!)
        if  let keybordInput = answer {
            
            switch keybordInput {
//               The user chooses the Combatant.
                case 1 :
                    let combatant = Combatant()
                    combatant.setName()
                    print("The name of the combatant is: \(combatant.name)")
                    combatant.typeSelected()
                    if (validateCharacter(gameCharacter: combatant, players: players )) {
                        self.teamMember.append(combatant)
                    }
                
//               The user chooses the Colossus.
                case 2 :
                    let colossus = Colossus()
                    colossus.setName()
                    print("The name of the colossus is: \(colossus.name)")
                    colossus.typeSelected()
                    if (validateCharacter(gameCharacter: colossus, players: players)) {
                        self.teamMember.append(colossus)
                    }
                
//                The user chooses the Dwarf.
                case 3 :
                    let dwarf = Dwarf()
                    dwarf.setName()
                    print("The name of the dwarf is: \(dwarf.name)")
                    dwarf.typeSelected()
                    if (validateCharacter(gameCharacter: dwarf, players: players)) {
                        self.teamMember.append(dwarf)
                    }
               
//               The user chooses the Magus.
                case 4 :
                    let magus = Magus()
                    magus.setName()
                    print("The name of the magus is: \(magus.name)")
                    magus.typeSelected()
                    if (validateCharacter(gameCharacter: magus, players: players)) {
                        self.teamMember.append(magus)
                    }
                
            default : print("Please enter a number between 1 and 4.")
            }
        }
        }while(teamMember.count < Config.characterLimit)
    }

    //   function allowing the selection of a unique name of the character.
    func validateCharacter(gameCharacter: Character,players: [Team]) -> Bool{

//   Accès au tableau teams
        for player in players  {
//            Access to the teamMember table in the teams table.
            for member in player.teamMember {
                if member.name == gameCharacter.name {
                    print("Please enter a single name for your character.")
                    return false
                }
            }
        }
    return true
    }
    
//  function allowing the selection of a character to attack or heal.
    func selectCharacter() -> Character  {
        var character = Character()
        var characterSelected = false
//       repeat as long as it's true.
        while !characterSelected {
             print("Please select a character to do an action")
             for i in 1...teamMember.count {
                print("\(i) - \(teamMember[i-1].name): \(Int(teamMember[i-1].life)) life points : \(Int(teamMember[i-1].weapon.damages)) damages : \(teamMember[i-1].characterType.name) type")
            }
            if let keybordInput = Int(readLine()!) {
                if keybordInput <= teamMember.count && keybordInput > 0 {
                    character = teamMember[keybordInput - 1]
                    characterSelected = true
                }else {
                    print("Please enter a number corresponding to the displayed characters.")
                    characterSelected = false
                }
            }
        }
    print("\(character.name) is selected")
    return character
    }
}



    

        







