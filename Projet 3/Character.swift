import Foundation
class Character {
    var name : String
    var life : Double
    var weapon = Weapon(name: "sword", damages: 15)
    var newWeapon = Weapon(name: "fire sword", damages: 30)
    var newWeaponChest = 0
    var characterType: Types = Fire()
    init() {
        name = ""
        life = 100.0
    }

    // function to nominate the characters.
    func setName() {
        repeat {
            print("Give a name to your character.")
            let entry = readLine()
            if let name = entry {
            self.name = name
            }
        }while self.name == ""
    }
    
    //    function to give the description of the characters.
    func description()  {
        print("Name: \(name) Life: \(Int(life)) Damages: \(Int(weapon.damages))")
    }

    //    Attack function against a character.
    func fight(against enemy: Character)  {
//        calculated damage versus type
            if characterType.name == enemy.characterType.weaknessesName {
            enemy.life -= weapon.damages  * characterType.strengthDamages
            
            }else if characterType.weaknessesName == enemy.characterType.name {
            
                enemy.life -= weapon.damages * characterType.weaknessesDamages
            
            }else {
//        simple calculated damage
            enemy.life -= weapon.damages
            }
      }
    
    
    
//   Choosing a random weapon.
    func newWeapon(character: Character)  {
        
        if newWeaponChest < Config.weaponLimit {
            character.weapon.damages += character.newWeapon.damages
            newWeaponChest += 1
            }else{
            print("At the moment of equipping your character realizes that he can only carry a weapon .. Sad, he rests the weapon in the chest !")
        }
    }
   
    func typeSelected() {
            
                
            print("Choose a type for your character: 1.Fire, 2.Wind, 3.Earth, 4.Water, 5.Electric")
    
            let answer = Int(readLine()!)
            if  let keybordInput = answer {
    
                switch keybordInput {
    //               The user chooses the Combatant.
                    case 1 :
                        print("The character type is: \(characterType.name)")
                    
    
    
    //               The user chooses the Colossus.
                    case 2 :
                        characterType = Wind()
                        print("The character type is: \(characterType.name)")
                    
    
    //                The user chooses the Dwarf.
                    case 3 :
                        characterType = Earth()
                        print("The character type is: \(characterType.name)")
                    
    
    
    //               The user chooses the Magus.
                    case 4 :
                        characterType = Electric()
                        print("The character type is: \(characterType.name)")
                    
                  
                    case 5 :
                        characterType = Water()
                        print("The character type is: \(characterType.name)")
                      
                    
                default : print("Please enter a number between 1 and 5.")
                }
            }
    }
}

