class Electric: Types {
    
    override init() {
        super .init()
        name = "electric"
        weaknessesName = "earth"
        strengthName = "water"
    }
}
